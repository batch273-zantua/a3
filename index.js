class Student {
    //to enable students instantiated from this class to have distinct names and emails, 
    //our constructor must be able to accept name and email arguments 
    //which it will then use to set the value of the object's corresponding properties
    constructor(name, email, grades) {
        //this.key = value/parameter
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;

        //Activity 1:
        if(grades.length === 4){
             if(grades.every(grade => grade >= 0 && grade <= 100)){
                 this.grades = grades;
             } else {
                 this.grades = undefined;
             }
         } else {
             this.grades = undefined;
         }
    }

    //Methods
    login() {
        console.log(`${this.email} has logged in.`);
        //console.log(this)
        return this;
    }

    logout() {
        console.log(`${this.email} has logged out.`);
        return this;
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}.`);
        return this;
    }

    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        //update property
        this.gradeAve = sum/4;
        //return object
        return this;
    }

    willPass() {
        if (this.computeAve() >= 85){
            console.log("passed: " + true);
        } else {
            console.log("passed : " + false);

        }
        return this;
    }

    willPassWithHonors() {
        if (this.willPass()) {
            if(this.computeAve() >= 90) {
                console.log("passed with honors : " + true);
            } else {
                console.log("passed with honors : " + false);
            }

        } else {
            console.log(undefined);
        }
        return this;
    }

}

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);


//1. classes
//2. Uppercase beginning letter
//3. new
//4. instantiate
//5. constructor

//Activity 2

//1. yes
//2. yes
//3. yes
//4. 
//5. object




